This is Java application. It was made at advanced Networks project.

This application provides:
Distributed peer-to-peer file sharing, where single file is divided into parts and distributed on many machines. Each machine will download its part of the file.

How it works:

1- Run the application, the moment it runs, you can contribute to downloading the torrent.
2- If you want to download a torrent, you first choose the torrent file.
3- The application will then search available peers to contribute to the torrent file.
4- After all chunks are downloaded, anyone can merge the chunks together to be able to run/open the file.
 