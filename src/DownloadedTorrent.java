import java.io.Serializable;

	public  class DownloadedTorrent implements Serializable {
		private Peer[] peers;
		private String torrentName;
		private boolean isCompleted;
		private boolean[] acks;
		private int numOfAcks;

		public DownloadedTorrent(Peer[] peers, String torrentName) {
			this.peers = peers;
			this.torrentName = torrentName;
			acks = new boolean[peers.length];
		}

		public void addAck(int index) {
			acks[index] = true;
			numOfAcks++;
		}

		public boolean isCompleted() {
			return numOfAcks == peers.length;
		}

		public String getTorrentName() {
			return torrentName;
		}

		public Peer[] getPeers() {
			return peers;
		}

	}

