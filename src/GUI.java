import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.FlowLayout;

public class GUI {

	private JFrame frame;
	PeerDiscovery mp;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public GUI() throws IOException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		// frame = new JFrame();
		// frame.setBounds(100, 100, 450, 300);
		// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.getContentPane().setLayout(new BorderLayout(0, 0));
		//
		// JPanel panel = new JPanel();
		// frame.getContentPane().add(panel, BorderLayout.NORTH);
		//
		// JButton downloadButton = new JButton("Download Torrent");
		// downloadButton.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// }
		// });
		// panel.add(downloadButton);
		//
		// JPanel panel_1 = new JPanel();
		// frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		// panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		// String[] selections = { "green", "red", "orange", "dark blue" };
		final UpdatableTableModel model = new UpdatableTableModel();
		JTable table = new JTable();
		table.setModel(model);
		table.getColumn("Status").setCellRenderer(new ProgressCellRender());
		// mergeButton = new MergeButtonRender("Merge");
		// mergeButton.setVisible(false);
		// mergeButton.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// System.out.println("killer");
		//
		// }
		// });
		table.getColumn("Merge").setCellRenderer(new MergeButtonRender("Merge"));
		table.addMouseListener(new JTableButtonMouseListener(table));
		// panel_1.add(table);
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		// frame.getContentPane().add(new JScrollPane(new
		// Button()),BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(table);
		frame.getContentPane().add(scrollPane, BorderLayout.SOUTH);

		int group = 6969;

		 mp = new PeerDiscovery(group, 6969, model);
		// Peer[] peers = mp.getPeers(1000, (byte) 0);

		JButton downloadBtn = new JButton("Download Torrent");
		downloadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					final File selectedFile = fileChooser.getSelectedFile();
					model.addFile(selectedFile.getName());
					model.updateMerge(selectedFile.getName(), false);
					new Thread() {
						public void run() {
							try {
								mp.downloadFile(selectedFile.getAbsolutePath(), selectedFile.getName());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						};
					}.start();

				}
			}
		});
		frame.getContentPane().add(downloadBtn, BorderLayout.NORTH);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		FileFinderWorker worker = new FileFinderWorker(model);
		// worker.execute();

	}

	public class ProgressCellRender extends JProgressBar implements TableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			int progress = 0;
			if (value instanceof Float) {
				progress = Math.round(((Float) value) * 100f);
			} else if (value instanceof Integer) {
				progress = (int) value;
			}
			setValue(progress);
			return this;
		}
	}

	public class MergeButtonRender extends JButton implements TableCellRenderer {
		public MergeButtonRender(String name) {
			super(name);
			this.setVisible(false);
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			System.out.println("Render =====" + value);
			if (value instanceof Boolean) {
				this.setVisible((boolean) value);
			}
			// setVisible(aFlag);
			return this;
		}

		@Override
		public void addActionListener(ActionListener l) {
			System.out.println("Pressed button");
			super.addActionListener(l);
		}

	}

	private class JTableButtonMouseListener extends MouseAdapter {
		private final JTable table;

		public JTableButtonMouseListener(JTable table) {
			this.table = table;
		}

		public void mouseClicked(MouseEvent e) {
			System.out.println("Mouse Clicked ");
			int column = table.getColumnModel().getColumnIndexAtX(e.getX()); // get
																				// the
																				// coloum
																				// of
																				// the
																				// button
			int row = e.getY() / table.getRowHeight(); // get the row of the
														// button

			/* Checking the row or column is valid or not */
			if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
				Object value = table.getValueAt(row, column);
				if (value instanceof JButton) {
					/* perform a click event */
//					
					String torrentName = table.getValueAt(row, 0).toString();
					try {
						synchronized (mp.hashTorrent) {
							if(mp != null && mp.hashTorrent != null && mp.hashTorrent.get(torrentName)!= null && mp.hashTorrent.get(torrentName).isCompleted()){
								mp.requestChunksFromPeers(torrentName);
							}else{
								((JButton) value).doClick();
							}
						}
						
					}  catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		}
	}

	public class FileFinderWorker extends SwingWorker<List<File>, File> {

		private UpdatableTableModel model;

		public FileFinderWorker(UpdatableTableModel model) {
			this.model = model;
		}

		@Override
		protected void process(List<File> chunks) {
			for (File file : chunks) {
				model.addFile(file.getAbsolutePath());
			}
		}

		@Override
		protected void done() {

		}

		@Override
		protected List<File> doInBackground() throws Exception {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public class FileReaderWorker extends SwingWorker<String, String> {

		private String currentFile;
		private UpdatableTableModel model;

		public FileReaderWorker(UpdatableTableModel model, String file) {
			this.currentFile = file;
			this.model = model;

			addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getPropertyName().equals("progress")) {
						FileReaderWorker.this.model.updateStatus(currentFile, (int) evt.getNewValue());
					}
				}
			});

		}

		@Override
		protected String doInBackground() throws Exception {
			System.out.println(currentFile);
			if (currentFile != null) {
				setProgress(23);
			} else {
				setProgress(10);
			}
			return currentFile;
		}
	}

}
