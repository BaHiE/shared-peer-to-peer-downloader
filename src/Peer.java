


import java.io.Serializable;
import java.net.InetAddress;


public class Peer implements Serializable {
	/**
	 * The ip of the peer
	 */
	public final InetAddress ip;

	/**
	 * The data of the peer
	 */
	public final int data;

	public Peer(InetAddress ip, int data) {
		this.ip = ip;
		this.data = data;
	}

	@Override
	public String toString() {

		return ip.getCanonicalHostName() + " " + data;
	}
}
