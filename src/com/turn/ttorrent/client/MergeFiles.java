package com.turn.ttorrent.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MergeFiles {
	 static byte[] read(String aInputFileName){
		 //   log("Reading in binary file named : " + aInputFileName);
		    File file = new File(aInputFileName);
		 
		    byte[] result = new byte[(int)file.length()];
		    try {
		      InputStream input = null;
		      try {
		        int totalBytesRead = 0;
		        input = new BufferedInputStream(new FileInputStream(file));
		        while(totalBytesRead < result.length){
		          int bytesRemaining = result.length - totalBytesRead;
		          //input.read() returns -1, 0, or more :
		          int bytesRead = input.read(result, totalBytesRead, bytesRemaining); 
		          if (bytesRead > 0){
		            totalBytesRead = totalBytesRead + bytesRead;
		          }
		        }
		        /*
		         the above style is a bit tricky: it places bytes into the 'result' array; 
		         'result' is an output parameter;
		         the while loop usually has a single iteration only.
		        */
		    
		      }
		      finally {
		       
		        input.close();
		      }
		    }
		    catch (FileNotFoundException ex) {
		     
		    }
		    catch (IOException ex) {
		     
		    }
		    return result;
		  }
	public static void main(String[] args) throws IOException {
		byte [] filea = read("/home/mohamed/Desktop/Medium1/1.mp3");
		byte [] fileb = read("/home/mohamed/Desktop/Medium2/2.mp3");
		BufferedOutputStream fileout = new BufferedOutputStream(new FileOutputStream(new File("/home/mohamed/merge2d.mp3")));
	
		fileout.write(filea);
		fileout.write(fileb);
		fileout.close();
	}
}
