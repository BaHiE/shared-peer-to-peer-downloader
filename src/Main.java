import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;

public class Main  {
	public static boolean deleteDirectory(File directory) {
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	    }
	    return(directory.delete());
	}
	public static byte [] readFile(File file){
		byte [] data = null ;
		try {
			data = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	public static void copyAny(File directory, File file){
		if(file.isDirectory()){
			System.out.println(directory.getPath()+"/"+file.getName());
			File newDir = new File(directory.getPath()+"/"+file.getName());
			if(!newDir.exists())
				newDir.mkdir();
			for(File subFile: file.listFiles()){
				copyAny(newDir, subFile);
			}
		}else{
			try {
				File newFile = new File(directory.getPath()+"/"+file.getName());
				if(!newFile.exists()){
					System.out.println("============ "+file.getAbsolutePath());
					newFile.createNewFile();
					FileOutputStream fs = new FileOutputStream(newFile,true);
					fs.write(readFile(file));
					fs.close();
				}else{
					
					byte [] allData = readFile(newFile);
					byte [] partData = readFile(file);
					FileOutputStream fs = new FileOutputStream(newFile);
					System.out.println("allData = "+allData.length+" partData = "+partData.length);
					byte [] resData = new byte [Math.max(allData.length,partData.length)]; 
					for (int i = 0;i< Math.min(allData.length,partData.length);i++){
						if(allData[i] == 0 && partData[i]!=0){
							resData[i] = partData[i];
						}else{
							resData[i] = allData[i];
						}
					}
					fs.write(resData);
					fs.close();
//					
//					for(int i = Math.min(allData.length, partData.length);i<resData.length;i++){
//						
//					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	public static void mergeFiles(String baseDir ,int parts){
		File directory = new File(baseDir);
		if(directory.exists())
			deleteDirectory(directory);
		directory.mkdir();
		for (int i = 0; i < parts;i++){
			File partDir = new File(baseDir+"_part"+(i));
			for(File file:partDir.listFiles()){
				copyAny(directory,file);
			}
		} 
	}
	public static void getInterfaces(){
		String ip;
	    try {
	        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
	        while (interfaces.hasMoreElements()) {
	            NetworkInterface iface = interfaces.nextElement();
	            // filters out 127.0.0.1 and inactive interfaces
	            if (iface.isLoopback() || !iface.isUp())
	                continue;

	            Enumeration<InetAddress> addresses = iface.getInetAddresses();
	            while(addresses.hasMoreElements()) {
	                InetAddress addr = addresses.nextElement();
	                
	                ip = addr.getHostAddress();
	                System.out.println(iface.getDisplayName() + " " + ip);
	            }
	        }
	    } catch (SocketException e) {
	        throw new RuntimeException(e);
	    }
	}
	public static void main(String[] args)
			throws UnknownHostException, IOException, NoSuchAlgorithmException, InterruptedException {
		System.out.println(InetAddress.getLocalHost());
		getInterfaces();
		mergeFiles("mmmm", 2);
		//int group = 1234;
	//	PeerDiscovery peerOp = new PeerDiscovery(group, group);
		
//		int numOfClients = Integer.parseInt(args[2]);
//		for(int i = 0 ;i<numOfClients ;i++){
//			File outDir = new File(args[1]+""+(i+1));
//			outDir.mkdir();
//			SharedTorrent t = SharedTorrent.fromFile(new File(args[0]),outDir);
//			t.setDownloadPecies(i, numOfClients);
//			for (String fileName : t.getFilenames())
//				System.out.println(fileName);
//			Client client = new Client(
//					// This is the interface the client will listen on (you might
//					// need something
//					// else than localhost here).
//					InetAddress.getLocalHost(),
//
//					// Load the torrent from the torrent file and use the given
//					// output directory. Partials downloads are automatically
//					// recovered.
//					t);
//			// You can optionally set download/upload rate limits
//			// in kB/second. Setting a limit to 0.0 disables rate
//			// limits.
//			client.setMaxDownloadRate(1000.0);
//			client.setMaxUploadRate(50.0);
//
//			// At this point, can you either call download() to download the torrent
//			// and
//			// stop immediately after...
//		
//			client.download();
//
//			// System.out.println(client.info());
//			client.addObserver(new Observer() {
//
//				@Override
//				public void update(Observable observable, Object data) {
//					Client client = (Client) observable;
//					float progress = client.getTorrent().getCompletion();
//					System.out.println("Progress "+progress);
//					System.out.println("BS R " + client.torrent.requestedPieces.toString());
//
//					System.out.println("State " + client.getState());
//				}
//
//			});
//			// Or call client.share(...) with a seed time in seconds:
//			// client.share(3600);
//			// Which would seed the torrent for an hour after the download is
//			// complete.
//
//			// Downloading and seeding is done in background threads.
//			// To wait for this process to finish, call:
//			client.waitForCompletion();
//			
//			//System.out.println("1");
//			// System.exit(0);
//			//System.out.println("2");
//			// At any time you can call client.stop() to interrupt the download
//		}
//		mergeFiles(args[1], numOfClients);

	}

}
