
public class RowData {
	private String fileName;
	private float status;
	private boolean couldMerge,merged;
	public boolean isCouldMerge() {
		return couldMerge;
	}
	public void setCouldMerge(boolean couldMerge) {
		this.couldMerge = couldMerge;
	}
	public boolean isMerged() {
		return merged;
	}
	public void setMerged(boolean merged) {
		this.merged = merged;
	}
	public RowData(String fileName){
		this.fileName = fileName;
	}
	public String getFileName(){
		return fileName;
	}
	public void setStatus(float status){
		this.status = status;
	}
	public float getStatus(){
		return status;
	}
	
}
