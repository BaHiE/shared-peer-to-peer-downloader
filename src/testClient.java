import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.Observable;
import java.util.Observer;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.common.Torrent;

public class testClient {
	public static void main(String[] args) throws UnknownHostException, IOException, NoSuchAlgorithmException {
		Client client = new Client(
				// This is the interface the client will listen on (you might
				// need something
				// else than localhost here).
				InetAddress.getLocalHost(),

				// Load the torrent from the torrent file and use the given
				// output directory. Partials downloads are automatically
				// recovered.
				SharedTorrent.fromFile(new File("/home/omar_arafa/tt.torrent"), new File("/home/omar_arafa/too")));
		
		// You can optionally set download/upload rate limits
		// in kB/second. Setting a limit to 0.0 disables rate
		// limits.
		client.setMaxDownloadRate(50.0);
		client.setMaxUploadRate(50.0);
		// At this point, can you either call download() to download the torrent
		// and
		// stop immediately after...
		client.download();
		client.getPeers();
		client.addObserver(new Observer() {
			  @Override
			  public void update(Observable observable, Object data) {
			    Client client = (Client) observable;
			    float progress = client.getTorrent().getCompletion();
			    System.out.println(client.getState());
			    System.out.println(client.getTorrent().getRequestedPieces());
			    System.out.println(progress);
			  }
			});
		// Or call client.share(...) with a seed time in seconds:
		// client.share(3600);
		// Which would seed the torrent for an hour after the download is
		// complete.

		// Downloading and seeding is done in background threads.
		// To wait for this process to finish, call:
		client.waitForCompletion();
	}
}
