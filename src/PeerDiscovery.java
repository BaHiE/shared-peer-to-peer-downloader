import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;

/**
 * Performs broadcast and multicast peer detection. How well this works depends
 * on your network configuration
 * 
 * @author ryanm
 */

public class PeerDiscovery {
	private static final byte QUERY_PACKET = 80;

	private static final byte RESPONSE_PACKET = 81;

	/**
	 * The group identifier. Determines the set of peers that are able to
	 * discover each other
	 */
	public final int group;

	/**
	 * The port number that we operate on
	 */
	public final int port;
	HashMap<String, DownloadedTorrent> hashTorrent;
	ArrayList<DownloadedTorrent> currentTorrents;
	UpdatableTableModel model;
	/**
	 * Data returned with discovery
	 */
	public int peerData;

	private final DatagramSocket bcastSocket;

	private final InetSocketAddress broadcastAddress;

	private boolean shouldStop = false;

	private boolean wantToParticipate = true;

	private List<Peer> responseList = null;
	private String okResponse = "200 OK";

	/**
	 * Used to detect and ignore this peers response to it's own query. When we
	 * send a response packet, we set this to the destination. When we receive a
	 * response, if this matches the source, we know that we're talking to
	 * ourselves and we can ignore the response.
	 */
	private InetAddress lastResponseDestination = null;

	/**
	 * Redefine this to be notified of exceptions on the listen thread. Default
	 * behaviour is to print to stdout. Can be left as null for no-op
	 */
	public ExceptionHandler rxExceptionHandler = new ExceptionHandler();

	private Thread bcastListen = new Thread(PeerDiscovery.class.getSimpleName() + " broadcast listen thread") {
		@Override
		public void run() {
			try {
				byte[] buffy = new byte[5];
				DatagramPacket rx = new DatagramPacket(buffy, buffy.length);

				while (!shouldStop) {
					try {
						buffy[0] = 0;

						bcastSocket.receive(rx);

						int recData = decode(buffy, 1);

						if (buffy[0] == QUERY_PACKET && recData == group && wantToParticipate) {
							byte[] data = new byte[5];
							data[0] = RESPONSE_PACKET;
							encode(peerData, data, 1);

							DatagramPacket tx = new DatagramPacket(data, data.length, rx.getAddress(), port);

							lastResponseDestination = rx.getAddress();

							bcastSocket.send(tx);
						} else if (buffy[0] == RESPONSE_PACKET) {
							if (responseList != null && !rx.getAddress().equals(lastResponseDestination)) {
								synchronized (responseList) {
									responseList.add(new Peer(rx.getAddress(), recData));
								}
							}
						}
					} catch (SocketException se) {
						// someone may have called disconnect()
					}
				}

				bcastSocket.disconnect();
				bcastSocket.close();
			} catch (Exception e) {
				if (rxExceptionHandler != null) {
					rxExceptionHandler.handle(e);
				}
			}
		};
	};

	/**
	 * Constructs a UDP broadcast-based peer
	 * 
	 * @param group
	 *            The identifier shared by the peers that will be discovered.
	 * @param port
	 *            a valid port, i.e.: in the range 1025 to 65535 inclusive
	 * @throws IOException
	 */
	public PeerDiscovery(int group, int port, UpdatableTableModel model) throws IOException {
		this.group = group;
		this.port = port;
		this.model = model;
		hashTorrent = new HashMap<String, DownloadedTorrent>();
		currentTorrents = new ArrayList<DownloadedTorrent>();

		bcastSocket = new DatagramSocket(port);
		broadcastAddress = new InetSocketAddress("255.255.255.255", port);

		bcastListen.setDaemon(true);
		bcastListen.start();
		receiver.setDaemon(true);
		receiver.start();
	}

	/**
	 * Signals this {@link PeerDiscovery} to shut down. This call will block
	 * until everything's timed out and closed etc.
	 */
	public void disconnect() {
		shouldStop = true;

		bcastSocket.close();
		bcastSocket.disconnect();

		try {
			bcastListen.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Queries the network and finds the addresses of other peers in the same
	 * group
	 * 
	 * @param timeout
	 *            How long to wait for responses, in milliseconds. Call will
	 *            block for this long, although you can
	 *            {@link Thread#interrupt()} to cut the wait short
	 * @param peerType
	 *            The type flag of the peers to look for
	 * @return The addresses of other peers in the group
	 * @throws IOException
	 *             If something goes wrong when sending the query packet
	 */
	public Peer[] getPeers(int timeout, byte peerType) throws IOException {
		responseList = new ArrayList<Peer>();

		// send query byte, appended with the group id
		byte[] data = new byte[5];
		data[0] = QUERY_PACKET;
		encode(group, data, 1);

		DatagramPacket tx = new DatagramPacket(data, data.length, broadcastAddress);

		bcastSocket.send(tx);

		// wait for the listen thread to do its thing
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
		}

		Peer[] peers;
		synchronized (responseList) {
			responseList.add(new Peer(InetAddress.getLocalHost(), 0));
			peers = responseList.toArray(new Peer[responseList.size()]);
		}

		responseList = null;

		return peers;
	}

	/**
	 * Handles an exception.
	 * 
	 * @author ryanm
	 */
	public class ExceptionHandler {
		/**
		 * Called whenever an exception is thrown from the listen thread. The
		 * listen thread should now be dead
		 * 
		 * @param e
		 */
		public void handle(Exception e) {
			e.printStackTrace();
		}
	}

	public void communicateSender(Socket socket, String fileName, int low, int high, String filePath) {
		try {
			System.out.println("Sending File ..." + fileName);
			File file = new File(filePath);
			// Get the size of the file
			String info = fileName + " " + low + " " + high + "\n\r";
			byte[] bytes = new byte[16 * 1024];
			InputStream in = new FileInputStream(file);
			OutputStream out = socket.getOutputStream();
			out.write(info.getBytes(), 0, info.getBytes().length);
			InputStream inSocket = socket.getInputStream();

			inSocket.read(bytes);
			System.out.println("Sending file to client ...");
			int count;
			while ((count = in.read(bytes)) > 0) {
				System.out.println("Sending bytes = " + count);
				out.write(bytes, 0, count);
			}
			out.close();
			in.close();
		} catch (Exception e) {

		}
	}

	public void communicateReceiver(Socket socket, String line) {
		try {
			System.out.println("Receiving Data ...");
			InputStream in = null;
			OutputStream out = null;
			try {
				in = socket.getInputStream();
			} catch (IOException ex) {
				System.out.println("Can't get socket input stream. ");
			}
			byte[] bytes = new byte[16 * 2048];
			// in.read(bytes);
			// String line = new String(bytes);
			// String[] result = line.split("\n\r");
			System.out.println("line = " + line);
			String[] info = line.split(" ");

			try {
				out = new FileOutputStream(info[0] + "_1");
			} catch (FileNotFoundException ex) {
				System.out.println("File not found. ");
			}
			System.out.println("hiiiiiiiiiiii");
			int count;
			while ((count = in.read(bytes)) > 0) {
				System.out.println("Receiving file bytes = " + count);
				out.write(bytes, 0, count);
			}
			out.close();
			in.close();
			InetSocketAddress socketAddress = (InetSocketAddress) socket.getRemoteSocketAddress();
			InetAddress ip = socketAddress.getAddress();
			socket.close();
			System.out.println("Done receving torrent");
			int one = Integer.parseInt(info[1]);
			int two = Integer.parseInt(info[2]);
			
			synchronized (hashTorrent) {
				if(!hashTorrent.containsKey(info[0].split("_")[0])){
					model.addFile(info[0].split("_")[0]);
				}
			}
			
			
			downloadChuncks(one, two, info[0] + "_1");

			// File completed
			System.out.println("File completed =D !!!!!");
			System.out.println(ip);
			Socket responseSocket = new Socket(ip, 3424);

			String recInfo = "ACK_RECEIVED" + " " + info[0] + " " + info[1] + "\n\r";

			OutputStream outResponse = responseSocket.getOutputStream();
			outResponse.write(recInfo.getBytes(), 0, recInfo.getBytes().length);
			outResponse.close();
			responseSocket.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void handleMessage(Socket socket) {
		try {
			InputStream in = socket.getInputStream();

			byte[] bytes = new byte[16 * 1024];
			in.read(bytes);
			String line = new String(bytes);
			line = line.split("\n\r")[0];
			System.out.println("[[[[[[[[[[" + line + "]]]]]]]]]]]]]]]");
			OutputStream out = socket.getOutputStream();
			// String[] result = line.split("\n\r");
			// String info = result;
			// AC_RECEIVED TORRENT ID
			if (line.split(" ")[0].equals("ACK_RECEIVED")) {
				System.out.println("ACk Received ..");
				String[] info = line.split(" ");
				String torrentName = info[1];
				int peerId = Integer.parseInt(info[2]);
				DownloadedTorrent curTorrent = null;
				synchronized (hashTorrent) {
					curTorrent = hashTorrent.get(torrentName);
				}
				curTorrent.addAck(peerId);
				if (curTorrent.isCompleted()) {
					Peer[] peers = curTorrent.getPeers();
					Socket[] peerSockets = new Socket[peers.length];
					int tcpPort = 3424;
					for (int i = 0; i < peers.length; i++) {
						peerSockets[i] = new Socket(peers[i].ip.getHostAddress(), tcpPort);
						sendPeers(peerSockets[i], curTorrent);
					}

					// synchronized (model) {
					// model.updateMerge(torrentName, true);
					// }
				}

			} else if (line.split(" ")[0].equals("Peers")) {
				receivePeer(socket);
			} else if (line.split(" ")[0].equals("REQUEST_FILE")) {
				sendChunks(socket, line.split(" ")[1]);
			} else {
				out.write(okResponse.getBytes(), 0, okResponse.getBytes().length);
				communicateReceiver(socket, line);
			}
			out.close();
			in.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void receivePeer(Socket socket) {
		try {
			System.out.println("Receiving Peers ...");
			OutputStream out = socket.getOutputStream();
			out.write(okResponse.getBytes(), 0, okResponse.getBytes().length);
			InputStream is = socket.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);
			DownloadedTorrent torrent = (DownloadedTorrent) ois.readObject();

			// if (torrent !=null){System.out.println(to.id);}
			synchronized (hashTorrent) {
				if (!hashTorrent.containsKey(torrent.getTorrentName())) {
					hashTorrent.put(torrent.getTorrentName(), torrent);
				}
			}

			is.close();
			out.close();
			socket.close();
			System.out.println("DONE");
		} catch (Exception e) {
		}
	}

	public void sendPeers(Socket socket, DownloadedTorrent curTorrent) {
		try {
			System.out.println("Sending Peers ...");
			String recInfo = "Peers\n\r";
			OutputStream os = socket.getOutputStream();
			os.write(recInfo.getBytes(), 0, recInfo.getBytes().length);
			// outResponse.close();

			byte[] bytes = new byte[16 * 1024];
			InputStream inSocket = socket.getInputStream();
			inSocket.read(bytes);

			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(curTorrent);
			oos.close();
			os.close();
			inSocket.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private Thread receiver = new Thread("Receiver Thread") {
		private int serverPort = 3424;

		public void run() {
			try {
				ServerSocket serverSocket = new ServerSocket(serverPort);
				while (!shouldStop) {
					final Socket socket = serverSocket.accept();
					new Thread(new Runnable() {
						@Override
						public void run() {
							handleMessage(socket);
							// communicateReceiver(socket);
						}

					}).start();
				}
				serverSocket.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
	};

	private static int decode(byte[] b, int index) {
		int i = 0;

		i |= b[index] << 24;
		i |= b[index + 1] << 16;
		i |= b[index + 2] << 8;
		i |= b[index + 3];

		return i;
	}

	private static void encode(int i, byte[] b, int index) {
		b[index] = (byte) (i >> 24 & 0xff);
		b[index + 1] = (byte) (i >> 16 & 0xff);
		b[index + 2] = (byte) (i >> 8 & 0xff);
		b[index + 3] = (byte) (i & 0xff);
	}

	// OLD CODE

	public void downloadChuncks(int index, final int numOfClients, final String torrentName)
			throws UnknownHostException, IOException, NoSuchAlgorithmException {

		File outDir = new File(torrentName.split("\\.")[0] + "_" + (index));
		outDir.mkdir();
		SharedTorrent t = SharedTorrent.fromFile(new File(torrentName), outDir);
		t.setDownloadPecies(index, numOfClients);
		for (String fileName : t.getFilenames())
			System.out.println(fileName);
		Client client = new Client(
				// This is the interface the client will listen on (you might
				// need something
				// else than localhost here).
				InetAddress.getLocalHost(),

				// Load the torrent from the torrent file and use the given
				// output directory. Partials downloads are automatically
				// recovered.
				t);
		// You can optionally set download/upload rate limits
		// in kB/second. Setting a limit to 0.0 disables rate
		// limits.
		client.setMaxDownloadRate(1000.0);
		client.setMaxUploadRate(50.0);

		// At this point, can you either call download() to download the torrent
		// and
		// stop immediately after...

		client.download();

		// System.out.println(client.info());
		client.addObserver(new Observer() {

			@Override
			public void update(Observable observable, Object data) {
				Client client = (Client) observable;
				float progress = client.getTorrent().getCompletion();
				System.out.println("Progress " + progress);
				System.out.println("BS R " + client.torrent.requestedPieces.toString());

				System.out.println("State " + client.getState());
				
				double start = 100.0-(100.0/numOfClients*1.0);
				synchronized (model) {
					System.out.println("to modellllllll");
					model.updateStatus(torrentName.split("_")[0], (int)(progress-start)*numOfClients);
				}
			}

		});
		// Or call client.share(...) with a seed time in seconds:
		// client.share(3600);
		// Which would seed the torrent for an hour after the download is
		// complete.

		// Downloading and seeding is done in background threads.
		// To wait for this process to finish, call:
		client.waitForCompletion();

		// System.out.println("1");
		// System.exit(0);
		// System.out.println("2");
		// At any time you can call client.stop() to interrupt the download

	}

	public void downloadFile(String filePath, String fileName)
			throws UnknownHostException, IOException, NoSuchAlgorithmException, InterruptedException {
		System.out.println(InetAddress.getLocalHost());
		Peer[] peers = getPeers(1000, (byte) 0);
		// arrayList
		DownloadedTorrent dt = new DownloadedTorrent(peers, fileName);
		currentTorrents.add(dt);
		synchronized (hashTorrent) {
			hashTorrent.put(fileName, dt);
		}

		System.out.println("Number of Peers=" + peers.length + " ....");
		Socket[] peerSockets = new Socket[peers.length];
		int tcpPort = 3424;
		for (int i = 0; i < peers.length; i++) {
			System.out.println("Sending to Peers ....");
			peerSockets[i] = new Socket(peers[i].ip.getHostAddress(), tcpPort);
			communicateSender(peerSockets[i], fileName, i, peers.length, filePath);

		}
		receiver.join();
		// mergeFiles(args[1], numOfClients);

	}

	public void requestChunksFromPeers(String torrentName) throws UnknownHostException, IOException {
		DownloadedTorrent curTorrent = null;
		synchronized (hashTorrent) {
			curTorrent = hashTorrent.get(torrentName);
		}

		for (int i = 0; i < curTorrent.getPeers().length; i++) {
			System.out.println("request file parts ....");
			int tcpPort = 3424;
			Socket socket = new Socket(curTorrent.getPeers()[i].ip.getHostAddress(), tcpPort);
			requestChunks(socket, torrentName, i);

		}
		Main.mergeFiles(torrentName.split("\\.")[0], curTorrent.getPeers().length);
	}

	private void requestChunks(Socket socket, String torrentName, int peerNum) {
		try {
			System.out.println("REQUEST FILE  ...");
			String recInfo = "REQUEST_FILE " + torrentName.split("\\.")[0] + "_" + peerNum + "\n\r";
			OutputStream os = socket.getOutputStream();
			os.write(recInfo.getBytes(), 0, recInfo.getBytes().length);
			// outResponse.close();

			byte[] bytes = new byte[16 * 1024];
			InputStream in = socket.getInputStream();

			OutputStream out = new FileOutputStream(torrentName.split("\\.")[0] + "_part" + peerNum + ".zip");
			int count;
			while ((count = in.read(bytes)) > 0) {
				System.out.println("Receiving file bytes = " + count);
				out.write(bytes, 0, count);
			}
			os.close();
			in.close();
			out.close();
			socket.close();
			UnZip.unZip(torrentName.split("\\.")[0] + "_part" + peerNum + ".zip");

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void sendChunks(Socket socket, String fileName) {
		try {
			System.out.println("Sending File ..." + fileName);
			ZipDirectory.zip(fileName);
			File file = new File(fileName + ".zip");
			// Get the size of the file
			// String info = fileName + " " + low + " " + high+"\n\r";
			byte[] bytes = new byte[16 * 1024];
			InputStream in = new FileInputStream(file);
			OutputStream out = socket.getOutputStream();
			// out.write(info.getBytes(), 0, info.getBytes().length);
			// InputStream inSocket = socket.getInputStream();

			// inSocket.read(bytes);
			System.out.println("Sending Zip file to  ...");
			int count;
			while ((count = in.read(bytes)) > 0) {
				System.out.println("Sending bytes = " + count);
				out.write(bytes, 0, count);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}