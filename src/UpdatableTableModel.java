
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class UpdatableTableModel extends AbstractTableModel {

	private List<RowData> rows;
	private Map<String, RowData> mapLookup;

	public UpdatableTableModel() {
		rows = new ArrayList<>(25);
		mapLookup = new HashMap<>(25);
	}

	@Override
	public int getRowCount() {
		return rows.size();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		String name = "??";
		switch (column) {
		case 0:
			name = "File Name";
			break;
		case 1:
			name = "Status";
			break;
		case 2:
			name = "Merge";
		}
		return name;
	}

	@Override
	public Object getValueAt(final int rowIndex, int columnIndex) {
		RowData rowData = rows.get(rowIndex);
		Object value = null;
		switch (columnIndex) {
		case 0:
			value = rowData.getFileName();
			break;
		case 1:
			value = rowData.getStatus();
			break;
		case 2:
			final JButton button = new JButton("Merge");
			((JButton) button).addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent((JButton) button),
							"File "+ getValueAt(rowIndex, 0).toString() +" not completed yet!");
				}
			});
			value = button;
			break;
		}
		return value;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		RowData rowData = rows.get(rowIndex);
		switch (columnIndex) {
		case 1:
			if (aValue instanceof Float) {
				rowData.setStatus((float) aValue);
			}
			break;
		case 2:
			if (aValue instanceof Boolean) {
//				rowData.setCouldMerge((boolean) aValue);
//				JButton b = (JButton)getValueAt(rowIndex, columnIndex);
//				b.setVisible(false);
			}
		}
	}

	public void addFile(String fileName) {
		RowData rowData = new RowData(fileName);
		mapLookup.put(fileName, rowData);
		rows.add(rowData);
		fireTableRowsInserted(rows.size() - 1, rows.size() - 1);
	}

	protected void updateStatus(String fileName, float progress) {
		System.out.println("IN  " + fileName);
		RowData rowData = mapLookup.get(fileName);
		if (rowData != null) {
			System.out.println("look success");
			int row = rows.indexOf(rowData);
			float p = (float) progress / 100f;
			setValueAt(p, row, 1);
			fireTableCellUpdated(row, 1);
		}
	}

	protected void updateMerge(String fileName, boolean merge) {
		RowData rowData = mapLookup.get(fileName);
		if (rowData != null) {
			System.out.println("bahieeeee      look success  " + merge);
			int row = rows.indexOf(rowData);
			// float p = (float) progress / 100f;
			setValueAt(merge, row, 2);
			fireTableCellUpdated(row, 2);
		}
	}
}